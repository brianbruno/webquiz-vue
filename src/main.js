import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import VueRouter from 'vue-router'
import axios from 'axios'
import VuetifyNotify from 'vuetify-notify';

import store from './store'
import router from './router/index'

Vue.use(VuetifyNotify, {
  vuetify,
  options: {
    timeout: 2500,
    toast: {
      x: "right",
      y: "bottom",
      // color: "error" // success, info, warning, error
    },
    dialog: {
      width: 400
    }
  }
});
Vue.use(VueRouter);

window.axios = axios;


Vue.config.productionTip = false;

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app');
