import Vue from 'vue'
import Vuex from 'vuex'
import router from "../router/index"

Vue.use(Vuex);

const urlDefault = 'http://webquizapi.brian.place';
//const urlDefault = 'http://localhost:80';
const equal = require('deep-equal');


export default new Vuex.Store({
    state: {
        salas: [],
        sala: {},
        usuario: {},
        requisicoes: {},
        estado: 'home',
        alteracoes: false,
        indiceSelecionado: 0,
    },

    getters: {

    },

    mutations: {
        criarSala(state, sala) {
            state.salas.push(sala)
        },

        criarUsuario(state, usuario) {
            state.usuario = usuario
        },

        setFazendoRequisicao(state, nomeRequisicao, status) {
            state.requisicoes[nomeRequisicao] = status;
        },

        setEstado(state, estado) {
          state.estado = estado;
        },

        setSala(state, sala) {
            state.sala = sala;
        },

        setAlteracoes(state, alteracao) {
            state.alteracoes = alteracao;
        },

        setUsuario(state, usuario) {
            state.usuario = usuario;
        },

        setComponentes(state, componentes) {
            state.sala.componentes = componentes;
        },

        limparSalas(state) {
            state.salas = [];
        },

        setIndice(state, indice) {
            state.indiceSelecionado = indice;
        }
    },

    actions: {

        criarSala(context, sala) {
            if (sala) {
                context.commit('setFazendoRequisicao', 'criarSala', true);
                // eslint-disable-next-line no-undef
                axios.post(urlDefault + '/sala', {
                    nome: sala,
                    criador: context.state.usuario._id
                }).then((response) => {
                    context.commit('criarSala', response.data);
                    context.commit('setFazendoRequisicao', 'criarSala', false);
                }).catch((error) => {
                    console.log(error);
                });
            }
        },

        criarUsuario(context, nome) {
            if (nome) {
                context.commit('setFazendoRequisicao', 'criarUsuario', true);
                // eslint-disable-next-line no-undef
                axios.post(urlDefault + '/usuario', {
                    nome: nome,
                }).then((response) => {
                    context.commit('criarUsuario', response.data);
                    context.commit('setEstado', 'sala');
                    context.commit('setFazendoRequisicao', 'criarUsuario', false);
                    localStorage.setItem('userId', response.data._id);
                }).catch((error) => {
                    console.log(error);
                });
            }
        },

        setUsuario(context, usuario) {
            context.commit('setUsuario', usuario);
        },

        setEstado(context, estado) {
            context.commit('setEstado', estado);
        },

        setSala(context, sala) {
            context.commit('setSala', sala);
        },

        setComponentes(context, componentes) {
            context.commit('setComponentes', componentes);
        },

        entrarEmSala(context, idSala) {
            const salasPresentes = (state, userId) => {
                let salasPresentes = [];
                state.salas.forEach((sala) => {
                    if (sala.participantes.includes(userId)) {
                        salasPresentes.push(sala._id);
                    }
                });
                return salasPresentes;
            };
            const salaJson = (state, idSala) => {
                let salaFinal = {};
                state.salas.forEach((salaForEach) => {
                    if (salaForEach._id === idSala) {
                        salaFinal = salaForEach;
                    }
                });

                return salaFinal;
            };

            const presencaSala = salasPresentes(context.state, context.state.usuario._id).includes(idSala);

            if (idSala && !presencaSala) {
                context.commit('setFazendoRequisicao', 'entrarEmSala', true);
                // eslint-disable-next-line no-undef
                axios.post(urlDefault + '/sala/' + idSala + '/user/' + context.state.usuario._id + '/entrar').then((response) => {

                    if (response.data.sucesso === false) {
                        console.log(response.data.mensagem);
                    } else {
                        context.commit('setEstado', 'emsala');
                        context.commit('setFazendoRequisicao', 'entrarEmSala', false);
                        context.commit('setSala', response.data);
                        router.push(`/sala/${idSala}`);
                    }

                }).catch((error) => {
                    console.log(error);
                });
            } else {
                if (presencaSala) {
                    context.commit('setSala', salaJson(context.state, idSala));
                    router.push(`/sala/${idSala}`);
                }
            }
        },

        buscarSala(context, salaId) {
            // eslint-disable-next-line no-undef
            return axios.get(urlDefault + '/sala/id/'+salaId);
        },

        buscarSalas(context) {
            const userId = localStorage.getItem('userId');
            if (userId) {
                // eslint-disable-next-line no-undef
                axios.get(urlDefault + `/sala/user/${userId}`,).then((response) => {
                    context.commit('limparSalas');
                    response.data.forEach((sala) => {
                        context.commit('criarSala', sala);
                    });
                }).catch((error) => {
                    console.log(error);
                });
            }
        },

        buscarUsuario(context) {
            // eslint-disable-next-line no-undef
            const userId = localStorage.getItem('userId');
            if (userId) {
                // eslint-disable-next-line no-undef
                axios.get(urlDefault + '/usuario/id/' + userId,).then((response) => {
                    if (response.data.sucesso) {
                        context.commit('setEstado', 'sala');
                        context.commit('setUsuario', response.data.usuario);
                    }
                }).catch((error) => {
                    console.log(error);
                });
            }
        },

        salvarComponentes(context) {
            const componentesAntesSalvar = context.state.sala.componentes;
            context.state.requisicoes['salvarComponentes'] = true;
            // eslint-disable-next-line no-undef
            axios.post(urlDefault + `/sala/${context.state.sala._id}/componente/`, { componentes: context.state.sala.componentes, nome: context.state.sala.nome }).then(() => {
                if (equal(componentesAntesSalvar, context.state.sala.componentes)) {
                    context.commit('setAlteracoes', false);
                }
                context.state.requisicoes['salvarComponentes'] = false;
            }).catch((error) => {
                console.log(error);
            });
        },

        iniciarApresentacao(context, idSala) {
            // eslint-disable-next-line no-undef
            return axios.post(urlDefault + `/sala/${idSala}/iniciarapresentacao/`, { });
        },

        finalizarApresentacao(context, idSala) {
            // eslint-disable-next-line no-undef
            return axios.post(urlDefault + `/sala/${idSala}/finalizarapresentacao/`, { });
        },

        selecionarIndiceComponente(context, data) {
            // eslint-disable-next-line no-undef
            return axios.post(urlDefault + `/sala/${data.idSala}/selecionarcomponente/${data.indice}`, { });
        },

        votarEnquete(context, data) {
            // eslint-disable-next-line no-undef
            return axios.post(urlDefault + `/sala/${data.idSala}/componente/${context.state.indiceSelecionado}/item/${data.item}`, { });
        }

    },

    modules: {}
})
