import Vue from 'vue'
import Router from 'vue-router'
import Home from "../pages/Home";
import Sala from "../pages/Sala";
import Apresentacao from "../pages/Apresentacao";
import Live from "../pages/Live";
import Login from "../pages/Login";

Vue.use(Router);

export default new Router({
    routes: [
        { path: '/', component: Home},
        { path: '/login', component: Login},
        { path: '/sala/:idsala', component: Sala},
        { path: '/live/:idsala', component: Apresentacao},
        { path: '/presentation/:idsala', component: Live},
    ]
})
